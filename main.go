package main

import (
	"fmt"
)

//COMPARE INTERFACE TO NIL
//reference : https://golang.org/doc/faq#nil_error
//Why is my nil error value not equal to nil?
//from that reference, we know that interface is nil only if its type & value is nil
type Iface interface {
	Fn()
}
type Foo struct{}

func (*Foo) Fn() {}
func isIfaceEqNil1(param Iface) bool {
	return param == (*Foo)(nil)
}
func isIfaceEqNil2(param Iface) bool {
	switch _param := param.(type) {
	case *Foo:
		return _param == nil
	}
	return false
}
func main() {
	var foo *Foo
	fmt.Printf("type: %T , value: %v\n", foo, foo)
	if isIfaceEqNil1(foo) {
		println("success1")
	} else {
		println("fail1")
	}
	if isIfaceEqNil2(foo) {
		println("success2")
	} else {
		println("fail2")
	}
}
